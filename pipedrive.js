const Bluebird = require('bluebird');
const Bottleneck = require('bottleneck');
const moment = require('moment');
const logger = require('../../../components/logger').logger();
const IntegrationHelper = require('../../../helpers/integration');
const IntegrationBase = require('../../components/integration-base');
const con = require('../../../configs/constants');
const CustomerImportableEntity = require('../entities/customer-importable-entity');
const OrganizationImportableEntity = require('../entities/organization-importable-entity');
const PipedriveActivityImportableEntity = require('../entities/pipedrive-activity-importable-entity');
const PipedriveDealImportableEntity = require('../entities/pipedrive-deal-importable-entity');
const Raven = require('raven');
const Handlebars = require('handlebars');
const fs = require('fs');
const Mapper = require('../../../helpers/mapper');
const MongoLogHelper = require('../../../helpers/log-helper');

module.exports = class Pipedrive extends IntegrationBase {
    /**
     * @property {ExportBaseHandler} export
     * @property {ImportBaseHandler} import
     * @param integrationRow
     */
    constructor(integrationRow) {
        super(integrationRow);
        this.updateOauth = this.updateOauth.bind(this);

        this.model = require('../models/initialize');
        
        this.PipedriveComponent = new (require('../../../components/pipedrive'))(
            integrationRow,
            this.updateOauth
        );

        this.export = new (require('./export-pipedrive-handler'))(
            integrationRow
        );
        this.import = new (require('./import-pipedrive-handler'))(
            integrationRow,
        );

        this.flowWorkerScheduled = this.flowWorkerScheduled.bind(this);
        this.getExternalAgents = this.getExternalAgents.bind(this);
        this.defineWebhooks = this.defineWebhooks.bind(this);
    }

    initialize() {
        return this.model.Company.findOne({
            where: {
                id: this.integrationRow.company_id,
            },
            include: {
                model: this.model.Country,
            },
        }).then(companyRow => {
            this.companyRow = companyRow;
            this.import.setCompanyRow(companyRow);
            this.export.setCompanyRow(companyRow);
            return this.PipedriveComponent.getRateLimit()
            .then(limits => {
                let apiRateLimiter = new Bottleneck({
                    id: 'pipedriveRestApi',
                    maxConcurrent: 25,
                    reservoir: limits.remaining, // initial value
                    reservoirRefreshAmount: limits.limit,
                    reservoirRefreshInterval: 2 * 1000, // must be divisible by 250
                });
                let pipedriveObjects = {
                    apiRateLimiter: apiRateLimiter,
                    PipedriveComponent: this.PipedriveComponent
                }
                this.import.initialize(pipedriveObjects);
                this.export.initialize(pipedriveObjects);
                this.PipedriveComponent.setApiRateLimiter(apiRateLimiter);
                this.apiRateLimiter = apiRateLimiter;

                return true;
            });
        });
    }

    /**
     * Updates access token in the database
     * 
     * @param {*} newOauth 
     * @author martin.kalivoda
     */
    updateOauth(newOauth) {
        let newConfig = this.integrationRow.config;
        newConfig.accessToken = newOauth.access_token;
        newConfig.tokenExpiration = newOauth.expires_in * 1000 + Date.now() // * 1000 because of converting seconds to miliseconds

        return this.model.Integration.update( // backward compatibility
            {
                config: JSON.stringify(newConfig),
                modified: moment().format('YYYY-MM-DD HH:mm:ss')
            },
            {
                where: {
                    id: this.integrationRow.id,
                },
            }
        ).then(() => {
            if (this.integrationRow.integration_ext_instance_id != null) {
                return this.model.IntegrationExtInstance.update(
                    {
                        access_token: newOauth.access_token,
                        token_expiration: newOauth.expires_in * 1000 + Date.now(), // * 1000 because of converting seconds to miliseconds
                        updated: moment().format('YYYY-MM-DD HH:mm:ss')
                    },
                    {
                        where: {
                            id: this.integrationRow.integration_ext_instance_id,
                        },
                    }
                ); 
            }
        }).then(() => {
            logger.info('Pipedrive access token refreshed in integration ' + this.integrationRow.id);
        });
    }

    /**
     * @returns {Promise}
     *
     * @author martin.kalivoda
     */
    flowIntegrationEnabled() {
        logger.info('FLOW INTEGRATION ENABLED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        this.setIntegrationStatus(con.INTEGRATION_STATUS_SYNCHRONIZING);
        this.refreshAgentsInRedis();
        return new Promise((resolve, reject) => {
            if ('mapContactsTo' in this.integrationRow.config && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                resolve(this.import.getPipedriveOrganizations());
            } else {
                resolve(this.import.getPipedriveContacts());
            }
        }).then(pipedriveEntities => {
                logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveEntities.length +
                    ' CONTACTS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                return Bluebird.map(
                    pipedriveEntities,
                    pipedriveEntity => {
                        if ('mapContactsTo' in this.integrationRow.config && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                            return this.import.createOrUpdateCustomer(
                                new OrganizationImportableEntity(pipedriveEntity, this.integrationRow.default_country_code, this.integrationRow.config.mapContactPhoneNumberTo, this.integrationRow.config.mapContactEmailTo),
                            );
                        } else {
                            return this.import.createOrUpdateCustomer(
                                new CustomerImportableEntity(pipedriveEntity, this.integrationRow.default_country_code),
                            );
                        }
                    },
                    {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                );
            })
            .then(() => logger.info("CONTACTS IMPORTED, INTEGRATION ID " + this.integrationRow.id))
            .then(() => {
                if (this.import.shouldImportTickets()) {
                    // Batch import tickets
                    return this.import.getPipedriveTickets().then(pipedriveTickets => {
                        logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveTickets.length + 
                            ' TICKETS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                        return Bluebird.map(
                            pipedriveTickets,
                            pipedriveTicket => {
                                return this.import.createOrUpdateActivity(
                                    new PipedriveActivityImportableEntity(pipedriveTicket, this.integrationRow),
                                );
                            },
                            {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                        );
                    })
                    .then(() => logger.info("TICKETS IMPORTED, INTEGRATION ID " + this.integrationRow.id));
                } else {
                    logger.warn('Importing of tickets is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                    return Promise.resolve();
                }
            })
            .then(() => {
                if (this.import.shouldImportDeals()) {
                    // Batch import deals
                    return this.import.getPipedriveDeals().then(pipedriveDeals => {

                        if (!pipedriveDeals) {
                            pipedriveDeals = [];
                        }

                        logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveDeals.length + 
                            ' DEALS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                        return Bluebird.map(
                            pipedriveDeals,
                            pipedriveDeal => {
                                return this.import.createOrUpdateActivity(
                                    new PipedriveDealImportableEntity(pipedriveDeal),
                                );
                            },
                            {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                        );
                    })
                    .then(() => logger.info("DEALS IMPORTED, INTEGRATION ID " + this.integrationRow.id));
                } else {
                    logger.warn('Importing of deals is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                    return Promise.resolve();
                }
            })
            .then(() => {
                return this.model.Integration.plugin.findIntegration({
                    id: this.integrationRow.id,
                    company_id: this.integrationRow.company_id
                }).then(integration => {
                    if (integration) {
                        return integration.integration_ext_instance_id;
                    } else {
                        throw new Error('Empty integration returned');
                    }
                })
                .then(this.defineWebhooks)
                .then((msg) => logger.info(msg))    
                .catch(err => {
                    this.log(err);
                })
            })
            .then(() => {
                return this.setIntegrationLastImport();
            })
            .catch(err => {
                this.log(err);
                Raven.captureException('Error while processing flowIntegrationEnabled ' + err);
                this.setIntegrationLastImport();
                throw err;
            });
    }

    /**
     * @returns {Promise}
     *
     * @author martin.kalivoda
     */
    flowWorkerScheduled() {
        let lastImport = this.integrationRow.last_import ? this.integrationRow.last_import : '1900-01-01 00:00:00';
        logger.info('FLOW WORKER SCHEDULED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        this.setIntegrationStatus(con.INTEGRATION_STATUS_SYNCHRONIZING); 
        // Batch import contacts or organizations
        return new Promise((resolve, reject) => {
            if ('mapContactsTo' in this.integrationRow.config && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                resolve(this.import.getPipedriveOrganizations(moment.tz(lastImport, 'Europe/Bratislava').tz('UTC').format()));
            } else {
                resolve(this.import.getPipedriveContacts(moment.tz(lastImport, 'Europe/Bratislava').tz('UTC').format()));
            }
        }).then(pipedriveEntities => {
                logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveEntities.length + 
                    ' CONTACTS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                return Bluebird.map(
                    pipedriveEntities,
                    pipedriveEntity => {
                        if ('mapContactsTo' in this.integrationRow.config && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                            return this.import.createOrUpdateCustomer(
                                new OrganizationImportableEntity(pipedriveEntity, this.integrationRow.default_country_code, this.integrationRow.config.mapContactPhoneNumberTo, this.integrationRow.config.mapContactEmailTo),
                            );
                        } else {
                            return this.import.createOrUpdateCustomer(
                                new CustomerImportableEntity(pipedriveEntity, this.integrationRow.default_country_code),
                            );
                        }
                    },
                    {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                );
            })
            .then(() => logger.info("CONTACTS IMPORTED, INTEGRATION ID " + this.integrationRow.id))
            .then(() => {
                if (this.import.shouldImportTickets()) {
                    // Batch import tickets
                    return this.import
                        .getPipedriveTickets(moment.tz(lastImport, 'Europe/Bratislava').tz('UTC').format())
                        .then(pipedriveTickets => {
                            logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveTickets.length +
                                ' TICKETS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                            return Bluebird.map(
                                pipedriveTickets,
                                pipedriveTicket => {
                                    return this.import.createOrUpdateActivity(
                                        new PipedriveActivityImportableEntity(pipedriveTicket, this.integrationRow),
                                    );
                                },
                                {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                            );
                        })
                        .then(() => logger.info("TICKETS IMPORTED, INTEGRATION ID " + this.integrationRow.id));
                } else {
                    logger.warn('Importing of tickets is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                    return;
                }
            })
            .then(results => {
                if (this.import.shouldImportDeals()) {
                    // Batch import deals
                    return this.import
                        .getPipedriveDeals(moment.tz(lastImport, 'Europe/Bratislava').tz('UTC').format())
                        .then(pipedriveDeals => {

                            if (!pipedriveDeals) {
                                pipedriveDeals = [];
                            }

                            logger.info('TRYING TO CREATE OR UPDATE ' + pipedriveDeals.length +
                                ' DEALS IN CLOUDTALK, INTEGRATION ID ' + this.integrationRow.id);
                            return Bluebird.map(
                                pipedriveDeals,
                                pipedriveDeal => {
                                    return this.import.createOrUpdateActivity(
                                        new PipedriveDealImportableEntity(pipedriveDeal),
                                    );
                                },
                                {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH},
                            );
                        })
                        .then(() => logger.info("DEALS IMPORTED, INTEGRATION ID " + this.integrationRow.id));
                } else {
                    logger.warn('Importing of deals is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                    return results;
                }
            })
            .then(() => {
                return this.setIntegrationLastImport();
            })
            .catch(e => {
                this.log(e);
                this.setIntegrationStatus(con.INTEGRATION_STATUS_FIRST_SYNCHRO_COMPLETED);
                throw e;
            });
    }

    /**
     * Removes existing webhooks and defines them again. This operation may be used 
     * to fix webhooks by resaving the integration.
     * 
     * @author martin.kalivoda
     */
    defineWebhooks() {
        return this.undefineWebhooks() // firstly remove defined webhooks if exist
        .then(() => {
            return this.model.IntegrationWebhook.destroy({
                where: {
                    company_id: this.integrationRow.company_id,
                    integration_ext_instance_id: this.integrationRow.integration_ext_instance_id
                }
            }).catch(this.log);
        })
        .then(() => {
            return Promise.all([
                this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/activity/create', 'added', 'activity'),
                this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/activity/update', 'updated', 'activity'),
                this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/deal/create', 'added', 'deal')
                // Update deal webhooks have incorrect currency value assigned to them, it always returns value: 0
                // this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/deal/update', 'updated', 'deal')
            ]).then(() => {
                if ('mapContactsTo' in this.integrationRow.config && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                    return this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/organization/update', 'updated', 'organization'); // this behaves also like organization added
                } else {
                    return this.defineWebhook(process.env.WEBHOOK_SUBSCRIPTION_URL_BASE + '/pipedrive/person/update', 'updated', 'person'); // this behaves also like person added
                }
            }).then(() => "WEBHOOKS DEFINED, INTEGRATION ID " + this.integrationRow.id);
        }).catch(this.log);
    }

    /**
     * Defines a webhook in the Pipedrive and inserts it into the database
     * 
     * @author martin.kalivoda
     */
    defineWebhook(subscriptionUrl, eventAction, eventObject) {
        let webhookParams = {
            subscription_url: subscriptionUrl,
            event_action: eventAction,
            event_object: eventObject
        };
        return this.apiRateLimiter.schedule(() => {
            return this.PipedriveComponent.oauthHttp('post', 'webhooks', null, webhookParams);
        }).then(response => {
            if (response.statusCode == 201) {
                let responseObj = {
                    statusCode: response.statusCode,
                    body: JSON.parse(response.body)
                }
                return this.model.IntegrationWebhook.plugin.create(
                    this.integrationRow.name + '_' + responseObj.body.data.id,
                    this.integrationRow.company_id,
                    this.integrationRow.integration_ext_instance_id,
                    Mapper.eventNameMappingExtToInt(responseObj.body.data.event_action, con.INTEGRATION_NAME_PIPEDRIVE),
                    Mapper.entityNameMappingExtToInt(responseObj.body.data.event_object, con.INTEGRATION_NAME_PIPEDRIVE)
                );
            } else {
                throw new Error('Error during webhook definition ' + subscriptionUrl);
            }
        })
    }

    flowWebhookContactCreatedOrUpdated(webhook) {
        logger.info('FLOW WEBHOOK CONTACT CREATED OR UPDATED LAUNCHED IN INTEGRATION ID ' + this.integrationRow.id);
        
        if (webhook.event === 'updated.organization' || webhook.event === 'added.organization') {
            if (this.integrationRow.config.hasOwnProperty('mapContactsTo') && this.integrationRow.config.mapContactsTo == con.PIPEDRIVE_ENTITY_ORGANIZATION) {
                return this.import.createOrUpdateCustomer(new OrganizationImportableEntity(webhook, this.integrationRow.default_country_code, this.integrationRow.config.mapContactPhoneNumberTo, this.integrationRow.config.mapContactEmailTo));
            } else {
                logger.warn('Import of Pipedrive organizations is restricted by configuration.');
                return;
            }
        } else {
            return this.import.createOrUpdateCustomer(new CustomerImportableEntity(webhook, this.integrationRow.default_country_code));
        }
    }

    flowWebhookTicketCreatedOrUpdated(webhook) {
        logger.info('FLOW WEBHOOK TICKET CREATED OR UPDATED LAUNCHED IN INTEGRATION ID ' + this.integrationRow.id);
        
        if (webhook.event === 'updated.deal' || webhook.event === 'added.deal') {
            return this.import.createOrUpdateActivity(new PipedriveDealImportableEntity(webhook));
        } else {
            return this.import.createOrUpdateActivity(new PipedriveActivityImportableEntity(webhook, this.integrationRow));
        }
    }

    /**
     * Gets Pipedrive users (aka agents) with attributes (external id, email).
     * This is overloaded function.
     * 
     * @returns {array}
     * 
     * @author martin.kalivoda
     */
    getExternalAgents() {
        return this.PipedriveComponent.oauthHttp('get', 'users')
        .then(users => {
            if (users.statusCode >= 200 && users.statusCode < 300) {
                let usersObj = {
                    statusCode: users.statusCode,
                    body: JSON.parse(users.body)
                };
                return Bluebird.map(usersObj.body.data, user => {
                    return {
                        id: user.id,
                        email: user.email,
                    };
                }, {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH});       
            } else {
                throw Error(JSON.stringify(users.toJSON()));
            }
        })
        .catch(err => {
            this.log(err);
            throw err;
        })
    }

    /**
     * This function is overloaded.
     * It deletes record in the table integration_ext_instances if should (implicitly removes 
     * webhooks from the Pipedrive side).
     * Also revokes tokens.
     * @author martin.kalivoda
     */
    deleteIntegrationExtInstance(shouldDeleteFromExtInstances) {
        if (shouldDeleteFromExtInstances) {
            return this.undefineWebhooks()
            .then(() => {
                return Promise.all([
                    this.PipedriveComponent.revokeAccessToken(),
                    this.PipedriveComponent.revokeRefreshToken(),
                    this.model.IntegrationExtInstance.plugin.delete(this.integrationRow.integration_ext_instance_id, this.integrationRow.company_id)
                ])
            });
        } else {
            return 0;
        }
    }

    /** 
     * Tries to remove all defined webhooks belonging to integration external instance ID
     * from external service
     * 
     * @author martin.kalivoda
     */
    undefineWebhooks() {
        return this.model.IntegrationWebhook.plugin.findAll({integration_ext_instance_id: this.integrationRow.integration_ext_instance_id})
        .then(webhooks => {
            return Bluebird.map(webhooks, webhook => {
                let webhookId = webhook.webhook_id.split('_')[1];
                return this.PipedriveComponent.oauthHttp('delete', 'webhooks/' + webhookId)
                .then(res => {
                    if (res.statusCode >= 200 && res.statusCode < 300) {
                        logger.info('Webhook ' + webhookId + ' successfully removed from Pipedrive, INTEGRATION ID ' + this.integrationRow.id);
                    } else {
                        logger.warn('Webhook ' + webhookId + ' was not successfully removed from Pipedrive, INTEGRATION ID ' + this.integrationRow.id);
                    }
                    return;
                })
            }, {concurrency: 3})
        })
        .catch(err => {
            this.log(err);
            return;
        })
    }
};
