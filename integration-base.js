const logger = require('../../components/logger').logger();
const con = require('../../configs/constants');
const IntegrationHelper = require('../../helpers/integration');
const moment = require('moment');
const Bluebird = require('bluebird');
const Raven = require('raven');
const sequelize = require('sequelize');
const AWS = require('aws-sdk');
const MongoLogHelper = require('../../helpers/log-helper');

// base class for an integration

/**
 * @property {ImportBaseHandler} import

 * @type {module.IntegrationBase}
 */
module.exports = class IntegrationBase {
    /**
     * @property {ExportBaseHandler} export
     * @property {ImportBaseHandler} import
     * @param integrationRow
     */
    constructor(integrationRow) {
        integrationRow.config = JSON.parse(integrationRow.config);
        this.integrationRow = integrationRow;

        this.sequelizeInstance = require('../../components/mysql').sequelize();

        this._model = {};

        this.log = this.log.bind(this);
    }

    /**
     * Inicializuje danu integraciu
     * @returns {Promise}
     */
    initialize() {
        return this.model.Company.findOne({
            where: {
                id: this.integrationRow.company_id,
            },
            include: {
                model: this.model.Country,
            },
        }).then(companyRow => {
            this.companyRow = companyRow;
            this.import.setCompanyRow(companyRow);
            this.export.setCompanyRow(companyRow);

            return true;
        });
    }

    /**
     * Processing of integration startup
     */
    processIntegrationEnabled() {
        throw Error('You have to implement the method processIntegrationEnabled');
    }

    /**
     * Processing of integration startup
     */
    processWorkerScheduled() {
        throw Error('You have to implement the method processWorkerScheduled');
    }

    /**
     * Scheduled worker processing
     *
     * @returns Promise
     */
    flowWorkerScheduled() {
        logger.info('FLOW WORKER SCHEDULED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return (
            this.setIntegrationStatus(con.INTEGRATION_STATUS_SYNCHRONIZING)
                .then(this.processWorkerScheduled)
                // empty then
                .then(() => {
                    return this.setIntegrationLastImport();
                })
                .catch(e => {
                    this.log(e);
                    return this.setIntegrationStatus(
                        con.INTEGRATION_STATUS_FIRST_SYNCHRO_COMPLETED,
                    );
                })
        )
        .then(() => logger.info('FLOW WORKER SCHEDULED COMPLETED, INTEGRATION ID ' + this.integrationRow.id))
        .catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * Processing of integration:enabled event
     *
     * @returns Promise
     */
    flowIntegrationEnabled() {
        logger.info('FLOW INTEGRATION ENABLED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return (
            this.setIntegrationStatus(con.INTEGRATION_STATUS_SYNCHRONIZING)
                .then(this.processIntegrationEnabled)
                // empty then
                .then(() => {
                    return this.setIntegrationLastImport();
                })
                .catch(e => {
                    Raven.captureException('Error while processing flowIntegrationEnabled ' + e);
                    return this.setIntegrationLastImport().then(() => {
                        return 'Error while processing flowIntegrationEnabled ' + e;
                    });

                })
        )
        .then(() => 'FLOW INTEGRATION ENABLED COMPLETED, INTEGRATION ID ' + this.integrationRow.id)
        .catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * Removes records from the tables integration_import_rows, integration_export_rows,
     * contacts_tags, integrations.
     * You can overload this method if you want to also remove webhooks.
     * 
     * @author martin.kalivoda
     */
    flowIntegrationDeleted(shouldDeleteFromExtInstances) {
        logger.info('FLOW INTEGRATION DELETED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return new Promise((resolve, reject) => {
            if (this.integrationRow.status == con.INTEGRATION_STATUS_DELETED) {
                return Promise.all([
                    this.model.IntegrationImportRow.plugin.delete(this.integrationRow.id, this.integrationRow.company_id),
                    this.model.IntegrationExportRow.plugin.delete(this.integrationRow.id, this.integrationRow.company_id),
                    this.model.ContactTag.plugin.delete({integration_id: this.integrationRow.id}),
                    this.model.Integration.plugin.delete(this.integrationRow.id, this.integrationRow.company_id)
                    .then(() => this.deleteIntegrationExtInstance(shouldDeleteFromExtInstances)),
                ]).then(() => {
                    resolve('FLOW INTEGRATION DELETED COMPLETED. INTEGRATION ID ' + this.integrationRow.id);
                });
            } else {
                reject(Error('flowIntegrationDeleted called, but integration status is ' + this.integrationRow.status));
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * When the call is created, the method checks the existence of contact (internal, external).
     * If internal contact does not exist, it does nothing.
     * If internal contact exists, it creates the contact and ticket in external service.
     * If external contact exists, it creates the ticket in external service.
     *
     * @param event
     * @param entityId
     * @returns {Promise}
     *
     * @author martin.kalivoda
     */
    flowCallCreated(event, entityId) {
        logger.info('FLOW CALL CREATED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        let contactId;

        return this.export
            .isExportOfCallAllowed(entityId)
            .then(resCallAllowed => {
                if (resCallAllowed.bool) {
                    return this.model.AsteriskCdr.plugin.getContactId(entityId)
                        .then(contact_id => {
                            contactId = contact_id;
                            return contactId;
                        })
                        .then(this.export.checkContactExistsInternal)
                        .then(existsInternal => {
                            if (existsInternal) {
                                return this.export.checkContactExistsExternal(contactId);
                            } else {
                                throw new Error('Internal contact does not exist');
                            }
                        })
                        .then(existsExternal => {
                            if (existsExternal) {
                                // Create External Ticket Only
                                return this.export.saveExternalTicket(
                                    event,
                                    entityId, 
                                    resCallAllowed.callData
                                )
                                .then(() => {
                                    return this.export.shouldProcessCallTag(entityId)
                                })
                                .then(should => {
                                    if (should) {
                                        return Promise.all([
                                            this.processCallTagsOfCdr(resCallAllowed.callData),
                                            this.processCallNotesOfCdr(resCallAllowed.callData)
                                        ]);
                                    } else {
                                        return;
                                    }
                                });
                            } else {
                                // Create External Contact First
                                return this.export.shouldExportContact(contactId)
                                .then(should => {
                                    if (should) {
                                        return (
                                            this.export
                                                .saveExportData(
                                                    con.INTEGRATION_EVENT_CONTACT_CREATED,
                                                    contactId,
                                                )
                                                .then(integrationExportRow => {
                                                    return this.export.saveExternalContact(
                                                        integrationExportRow,
                                                    );
                                                })
                                                // Then Create External Ticket
                                                .then(() => {
                                                    return this.export.saveExternalTicket(
                                                        event,
                                                        entityId, 
                                                        resCallAllowed.callData
                                                    );
                                                })
                                                .then(() => {
                                                    return this.export.shouldProcessCallTag(entityId)
                                                })
                                                .then(should => {
                                                    if (should) {
                                                        return Promise.all([
                                                            this.processCallTagsOfCdr(resCallAllowed.callData),
                                                            this.processCallNotesOfCdr(resCallAllowed.callData)
                                                        ]);
                                                    } else {
                                                        return;
                                                    }
                                                })
                                        );
                                    } else {
                                        logger.warn('Exporting of an internal lead is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                                        return;
                                    }
                                })
                            }
                        });
                } else {
                    MongoLogHelper.updateStatus(this.integrationRow, con.LOG_CALL_EXPORT_RESTRICTED_BY_CONFIGURATION);
                    logger.warn(
                        'Exporting of the call with id ' +
                        entityId +
                        ' is restricted by the configuration. INTEGRATION ID ' + this.integrationRow.id
                    );
                    return;
                }
            })
            .then(() =>  'FLOW CALL CREATED COMPLETED, INTEGRATION ID ' + this.integrationRow.id)
            .catch(err => {
                this.log(err);
                throw err;
            });
    }

    /**
     * @param event
     * @param entityId
     * @returns {Promise}
     *
     * @author martin.kalivoda
     */
    flowContactEdited(event, entityId) {
        logger.info('FLOW CONTACT EDITED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return this.export.shouldExportContact(entityId)
        .then(should => {
            if (should) {
                return this.export
                    .checkContactExistsExternal(entityId) // if exists, external id is returned
                    .then(existsExternal => {
                        if (existsExternal) {
                            return this.export
                                .saveExportData(event, entityId)
                                .then(integrationExportRow => {
                                    return this.export.saveExternalContact(integrationExportRow);
                                });
                        } else {
                            return this.export
                                .saveExportData(con.INTEGRATION_EVENT_CONTACT_CREATED, entityId)
                                .then(integrationExportRow => {
                                    return this.export.saveExternalContact(integrationExportRow);
                                });
                        }
                    });
            } else {
                logger.warn('Exporting of an internal lead is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        })
        .then(() => 'FLOW CONTACT EDITED COMPLETED, INTEGRATION ID ' + this.integrationRow.id)
        .catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * @param event
     * @param entityId
     * @returns {Promise}
     *
     * @author martin.kalivoda
     */
    flowContactCreated(event, entityId) {
        logger.info('FLOW CONTACT CREATED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        let shouldExportCalls;
        return this.export.shouldExportContact(entityId)
        .then(should => {
            if (should) {
                logger.debug('Contact should be exported, INTEGRATION ID ' + this.integrationRow.id);
                return this.export
                    .checkContactExistsInternal(entityId)
                    .then(existsInternal => {
                        if (existsInternal) {
                            logger.debug('Contact exists internal, INTEGRATION ID ' + this.integrationRow.id);
                            return this.export.checkContactExistsExternal(entityId)
                            .then(externalContact => {
                                if (externalContact) {
                                    logger.debug('Contact exists in external service, INTEGRATION ID ' + this.integrationRow.id);
                                    shouldExportCalls = false;
                                    return this.export.saveExportData(
                                        con.INTEGRATION_EVENT_CONTACT_EDITED, 
                                        entityId
                                    );
                                } else {
                                    // shouldExportCalls = true; Mustn't export calls as batch
                                    logger.debug('Contact does not exists in external service, INTEGRATION ID ' + this.integrationRow.id);
                                    shouldExportCalls = false;
                                    return this.export.saveExportData(
                                        event,
                                        entityId,
                                    );
                                }
                            })
                            .then(integrationExportRow => {
                                return this.export.saveExternalContact(integrationExportRow);
                            });
                        } else {
                            throw new Error('Internal contact does not exist');
                        }
                    })
                    .then(() => 'FLOW CONTACT CREATED COMPLETED, INTEGRATION ID ' + this.integrationRow.id);
            } else {
                logger.warn('Exporting of a new internal lead is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * This method is overloaded in the child class
     * 
     * @author martin.kalivoda
     */
    defineWebhook(subscriptionUrl, eventAction, eventObject) {
        throw new Error('You have to implement the method defineWebhook');
    }

    /**
     * This method is overloaded in the child class
     * 
     * @uathor martin.kalivoda
     */
    undefineWebhook() {
        throw new Error('You have to implement the method undefineWebhook');
    }

    /**
     * This method must be overloaded
     * @param {*} webhook 
     */
    flowWebhookContactCreatedOrUpdated(webhook) {
        throw new Error('You have to implement the method flowWebhookContactCreatedOrUpdate')
    }

    /**
     * This method must be overloaded
     * @param {*} webhook 
     */
    flowWebhookTicketCreatedOrUpdated(webhook) {
        throw new Error('You have to implement the method flowWebhookTicketCreatedOrUpdated')
    }

    flowCallTagCreated(event, callTagId, cdrId) {
        // find out if tag can be exported
        // add row to integration_activity_attributes
        // save export data
        // prepare tag exportable entity
        // export tag
        // update export data
        // update integration_activity_attributes data
        logger.info('FLOW CALL TAG CREATED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return this.export.shouldProcessCallTag(cdrId)
        .then(should => {
            if (should) {
                return this.export.saveActivityAttribute(callTagId, con.ATTRIBUTE_TYPE_CALL_TAG, cdrId)
                .then(activityAttributeRows => {
                    return Bluebird.map(activityAttributeRows, activityAttributeRow => {
                        logger.debug('activityAttributeRow: ' + JSON.stringify(activityAttributeRow));
                        let integrationExportRow;
                        let integrationActivityAttributeRow = activityAttributeRow;
                        return Promise.all([
                            this.export.saveExportData(
                                event,
                                integrationActivityAttributeRow.id,
                            ),
                            this.export.prepareCallTagDataToCreate(integrationActivityAttributeRow)
                        ]) 
                        .then(data => {
                            logger.debug('I have tag data: ' + JSON.stringify(data[1]));
                            integrationExportRow = data[0];
                            let tagData = data[1];
                            this.export.updateRequestDataInExportData(tagData, integrationExportRow.id);
                            return this.export.saveExternalCallTag(tagData);
                        })
                        .then(exportedEntity => {
                            logger.debug('exported entity is: ' + JSON.stringify(exportedEntity));
                            logger.debug('exportedEntity.result is: ' + exportedEntity.result);
                            logger.debug('integrationActivityAttributeRow.id is :' + integrationActivityAttributeRow.id);
                            if (exportedEntity.result == con.EXPORT_ENTITY_STATUS_ERROR) {
                                this.export.updateExportData(exportedEntity, integrationExportRow.id)
                                return new Error(JSON.stringify(exportedEntity));
                            } else {
                                return Promise.all([
                                    this.export.updateActivityAttribute(exportedEntity, integrationActivityAttributeRow.id, con.INTEGRATION_ACTIVITY_ATTRIBUTE_STATUS_EXPORTED),
                                    this.export.updateExportData(exportedEntity, integrationExportRow.id)
                                ]); 
                            }  
                        });
                    }, {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH});
                })
                .then(() => 'FLOW CALL TAG CREATED COMPLETED, INTEGRATION ID ' + this.integrationRow.id);
            } else {
                logger.warn('Exporting of a new call tag is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    flowCallTagDeleted(event, callTagId, cdrId) {
        // find out if tag can be deleted
        // add row to integration_activity_attributes
        // save export data
        // prepare tag exportable entity
        // export tag
        // update export data
        // update integration_activity_attributes data
        logger.info('FLOW CALL TAG DELETED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return this.export.shouldProcessCallTag(cdrId)
        .then(should => {
            if (should) {
                return this.export.setIntegrationActivityAttributeStatus(con.INTEGRATION_ACTIVITY_ATTRIBUTE_STATUS_DELETED, callTagId, con.ATTRIBUTE_TYPE_CALL_TAG)
                .then(() => {
                    return this.export.getActivityAttribute(callTagId, con.ATTRIBUTE_TYPE_CALL_TAG)
                })
                .then(activityAttributes => {
                    if (activityAttributes.length == 0) {
                        logger.warn('There is no integration_activity_attribute row returned for call tag ' + callTagId + ' and integration id ' + this.integrationRow.id);
                    }
                    return Bluebird.map(activityAttributes, activityAttribute => {
                        let integrationExportRow;
                        let integrationActivityAttributeRow = activityAttribute;
                        return Promise.all([
                            this.export.saveExportData(
                                event,
                                integrationActivityAttributeRow.id,
                            ),
                            this.export.prepareCallTagDataToDelete(integrationActivityAttributeRow)
                        ])
                        .then(data => {
                            logger.debug('I have call tag data to delete: ' + JSON.stringify(data[1]));
                            integrationExportRow = data[0];
                            let tagData = data[1];
                            this.export.updateRequestDataInExportData(tagData, integrationExportRow.id);
                            return this.export.deleteExternalCallTag(tagData);
                        })
                        .then(responseEntity => {
                            let updatedData = {
                                cdr_id: cdrId,
                                call_tag_id: callTagId,
                                ...responseEntity.data.response,
                            }
                            responseEntity.data = updatedData;
                            logger.debug('response entity is: ' + JSON.stringify(responseEntity));
                            if (responseEntity.result == con.EXPORT_ENTITY_STATUS_ERROR) {
                                return this.export.updateExportData(responseEntity, integrationExportRow.id);    
                            } else {
                                return Promise.all([
                                    this.export.deleteActivityAttribute(integrationActivityAttributeRow),
                                    this.export.updateExportData(responseEntity, integrationExportRow.id)
                                ]); 
                            }  
                        })
                    }, {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH})
                })
                .then(() => 'FLOW CALL TAG DELETED COMPLETED, INTEGRATION ID ' + this.integrationRow.id);
            } else {
                logger.warn('Deleting of a call tag is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * Processes, e.g. export, call tags of recently exported call.
     * @author martin.kalivoda
     */
    processCallTagsOfCdr(asteriskCdrRow) {
        let callTagsData = this.export.getCallTagsFromCdr(asteriskCdrRow);
        
        if (callTagsData) {
            return Bluebird.map(callTagsData.ids, callTagId => {
                return this.flowCallTagCreated(con.INTEGRATION_EVENT_CALL_TAG_CREATED, callTagId, asteriskCdrRow.id);
            }, {concurrency: 1}); // synchronous manner because some integrations make duplicities of tags, e.g. Freshdesk
        }
    }

    /**
     * Base method for processing call note created event
     * 
     * @author martin.kalivoda
     */
    flowCallNoteCreated(event, callNoteId, cdrId) {
        // find out if call note can be exported
        // add row to integration_activity_attributes
        // save export data
        // prepare call note exportable entity
        // export call note
        // update export data
        // update integration_activity_attributes data
        logger.info('FLOW CALL NOTE CREATED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        return this.export.shouldProcessCallNote(cdrId, event)
        .then(should => {
            if (should) {
                return this.export.saveActivityAttribute(callNoteId, con.ATTRIBUTE_TYPE_CALL_NOTE, cdrId)
                .then(activityAttributeRows => {
                    return Bluebird.map(activityAttributeRows, activityAttributeRow => {
                        logger.debug('activityAttributeRow: ' + JSON.stringify(activityAttributeRow));
                        let integrationExportRow;
                        let integrationActivityAttributeRow = activityAttributeRow;
                        return Promise.all([
                            this.export.saveExportData(
                                event,
                                integrationActivityAttributeRow.id,
                            ),
                            this.export.prepareCallNoteDataToCreate(integrationActivityAttributeRow)
                        ]) 
                        .then(data => {
                            logger.debug('I have call note data: ' + JSON.stringify(data[1]));
                            integrationExportRow = data[0];
                            let noteData = data[1];
                            this.export.updateRequestDataInExportData(noteData, integrationExportRow.id);
                            return this.export.saveExternalCallNote(noteData);
                        })
                        .then(exportedEntity => {
                            logger.debug('exported entity is: ' + JSON.stringify(exportedEntity));
                            logger.debug('exportedEntity.result is: ' + exportedEntity.result);
                            logger.debug('integrationActivityAttributeRow.id is :' + integrationActivityAttributeRow.id);
                            if (exportedEntity.result == con.EXPORT_ENTITY_STATUS_ERROR) {
                                this.export.updateExportData(exportedEntity, integrationExportRow.id)
                                return new Error(JSON.stringify(exportedEntity));
                            } else {
                                return Promise.all([
                                    this.export.updateActivityAttribute(exportedEntity, integrationActivityAttributeRow.id, con.INTEGRATION_ACTIVITY_ATTRIBUTE_STATUS_EXPORTED),
                                    this.export.updateExportData(exportedEntity, integrationExportRow.id)
                                ]); 
                            }  
                        });
                    }, {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH});
                })
                .then(() => 'FLOW CALL NOTE CREATED COMPLETED, INTEGRATION ID ' + this.integrationRow.id);
            } else {
                logger.warn('Exporting of a new call note is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * Base method for processing call note edited event
     * 
     * @author martin.kalivoda
     */
    flowCallNoteEdited(event, callNoteId, cdrId) {
        logger.info('FLOW CALL NOTE EDITED LAUNCHED, INTEGRATION ID ' + this.integrationRow.id);
        let responseEntityResult;

        return this.export.shouldProcessCallNote(cdrId, event)
        .then(should => {
            if (should) {
                return this.export.getActivityAttribute(callNoteId, con.ATTRIBUTE_TYPE_CALL_NOTE)
                .then(activityAttributes => {
                    if (activityAttributes.length == 0) {
                        logger.warn('There is no integration_activity_attribute row returned for call note ' + callNoteId + ' and integration id ' + this.integrationRow.id);
                    }
                    return Bluebird.map(activityAttributes, activityAttribute => {
                        let integrationExportRow;
                        let integrationActivityAttributeRow = activityAttribute;
                        return Promise.all([
                            this.export.saveExportData(
                                event,
                                integrationActivityAttributeRow.id,
                            ),
                            this.export.prepareCallNoteDataToEdit(integrationActivityAttributeRow)
                        ])
                        .then(data => {
                            logger.debug('I have call note data to edit: ' + JSON.stringify(data[1]));
                            integrationExportRow = data[0];
                            let noteData = data[1];
                            this.export.updateRequestDataInExportData(noteData, integrationExportRow.id);
                            return this.export.editExternalCallNote(noteData);
                        })
                        .then(responseEntity => {
                            let updatedData = {
                                cdr_id: cdrId,
                                call_note_id: callNoteId,
                                ...responseEntity.data.response,
                            }
                            responseEntity.data = updatedData;
                            logger.debug('response entity is: ' + JSON.stringify(responseEntity));
                            responseEntityResult = responseEntity.result
                            return this.export.updateExportData(responseEntity, integrationExportRow.id);    
                        })
                    }, {concurrency: con.BLUEBIRD_CONCURRENCY_HIGH})
                })
                .then(() => {
                    if (responseEntityResult == con.EXPORT_ENTITY_STATUS_DONE) {
                        return this.export.saveActivityAttribute(callNoteId, con.ATTRIBUTE_TYPE_CALL_NOTE, cdrId, con.INTEGRATION_ACTIVITY_ATTRIBUTE_STATUS_EXPORTED);
                    } else {
                        return 0;
                    }
                })
                .then(() => 'FLOW CALL NOTE EDITED COMPLETED, INTEGRATION ID ' + this.integrationRow.id);
            } else {
                logger.warn('Editing of a call note is restricted by the configuration, INTEGRATION ID ' + this.integrationRow.id);
                return;
            }
        }).catch(err => {
            this.log(err);
            throw err;
        });
    }

    /**
     * Processes, e.g. export, call notes of recently exported call.
     * @author martin.kalivoda@cloudtalk.io
     */
    processCallNotesOfCdr(asteriskCdrRow) {
        let callNotesData = this.export.getCallNotesFromCdr(asteriskCdrRow);
        
        if (callNotesData) {
            return Bluebird.map(callNotesData.ids, callNoteId => {
                return this.flowCallNoteCreated(con.INTEGRATION_EVENT_CALL_NOTE_CREATED, callNoteId, asteriskCdrRow.id);
            }, {concurrency: 1}); // synchronous manner because some integrations make duplicities of tags, e.g. Freshdesk
        }
    }

    /**
     * Sets required status to integration and its parallel instances. 
     * If the integration is in the status 4 - deleted, nothing will change
     * @param {*} status
     */
    setIntegrationStatus(status) {
        return this.model.Integration.plugin.update({
            status: status,
            modified: moment().format(moment().format('YYYY-MM-DD HH:mm:ss'))
        }, {
            integration_ext_instance_id: this.integrationRow.integration_ext_instance_id,
            company_id: this.integrationRow.company_id,
            status: {[this.sequelizeInstance.Op.notIn]: [con.INTEGRATION_STATUS_DELETED, con.INTEGRATION_STATUS_SETUP_INCOMPLETE, con.INTEGRATION_STATUS_INACTIVE]}
        });
    }

    /**
     * Sets last_import date/time and status 'synchronization completed' to all parallel integrations
     * @returns {Promise.<this>}
     */
    setIntegrationLastImport() {
        return this.model.Integration.plugin.update({
            status: con.INTEGRATION_STATUS_FIRST_SYNCHRO_COMPLETED,
            last_import: moment().format('YYYY-MM-DD HH:mm:ss'),
            modified: moment().format(moment().format('YYYY-MM-DD HH:mm:ss'))
        },{
            integration_ext_instance_id: this.integrationRow.integration_ext_instance_id,
            status: con.INTEGRATION_STATUS_SYNCHRONIZING
        });
    }

    /**
     * Refreshes agents from external service in Redis storage
     * 
     * @author martin.kalivoda
     */
    refreshAgentsInRedis() {
        return this.getExternalAgents()
        .then(agents => {
            logger.debug('in refreshAgentsInRedis, agents: ' + JSON.stringify(agents));
            if (IntegrationHelper.isNotEmpty(agents)) {
                redisComponent.setAgents(agents, this.integrationRow);
            }
            return 0;
        })
        .catch(err => {
            this.log(err);
        });        
    }

    /**
     * Checks pending status based on the modified field. If the difference between now and modified
     * is more than 24 hours, integration status will be changed from 3 to 2.
     * 
     * @author martin.kalivoda
     */
    checkAndChangePendingStatus() {
        return new Promise((resolve, reject) => {
            logger.info('In check and change pending status, INTEGRATION ID ' + this.integrationRow.id);
            let now = moment().format('X');
            let modified = moment(this.integrationRow.modified).format('X');
            if (now - modified > moment('1970-01-02').format('X')) { // more than one day
                return this.model.Integration.plugin.update({
                    status: 2,
                    modified: moment().format('YYYY-MM-DD HH:mm:ss')
                }, {
                    id: this.integrationRow.id,
                    company_id: this.integrationRow.company_id
                }).then(updatedRowsCount => {
                    logger.info('Status 3 changed to 2, INTEGRATION ID ' + this.integrationRow.id);
                    return updatedRowsCount;
                });
            } else {
                resolve(0);
            }
        });
    }

    /**
     * Checks and change status 'setup incomplete' of the integration to status 'deleted'
     * and sends SQS message with integration:deleted event 
     */
    checkAndChangeSetupIncompleteStatus() {
        return new Promise((resolve, reject) => {
            let now = moment().format('X');
            let modified = moment(this.integrationRow.modified).format('X');
            if (now - modified > moment('1970-01-01 01:00:00').format('X') && // more than 1 hour
                this.integrationRow.status == con.INTEGRATION_STATUS_SETUP_INCOMPLETE
            ) { // more than one hour
                return this.model.Integration.plugin.update({
                    status: 4,
                    modified: moment().format('YYYY-MM-DD HH:mm:ss')
                }, {
                    id: this.integrationRow.id,
                    company_id: this.integrationRow.company_id
                }).then(() => {
                    logger.info('Status -1 changed to 4, INTEGRATION ID ' + this.integrationRow.id);
                    const sqs = new AWS.SQS({
                        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
                        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
                        region: process.env.AWS_REGION,
                    });

                    const params = {
                        MessageAttributes: {},
                        MessageBody: JSON.stringify(
                            // INTEGRATION DELETED FLOW
                            {
                                event: 'integration:deleted',
                                company_id: this.integrationRow.company_id,
                                integration_id: this.integrationRow.id
                            }
                        ),
                        QueueUrl: process.env.AWS_QUEUE_URL
                    };

                    sqs.sendMessage(params, function (err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                })
            } else {
                resolve();
            }
        })
    }

    getExternalAgents() {
        throw Error('You have to implement the method getExternalAgents');        
    }

    /**
     * Deletes record in the table integration_ext_instances if should
     * @author martin.kalivoda
     */
    deleteIntegrationExtInstance(shouldDeleteFromExtInstances) {
        if (shouldDeleteFromExtInstances) {
            return this.model.IntegrationExtInstance.plugin.delete(this.integrationRow.integration_ext_instance_id, this.integrationRow.company_id);
        } else {
            return 0;
        }
    }
};
